# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import purchase
from . import sale_cost


def register():
    Pool.register(
        sale_cost.SaleCost,
        sale_cost.CostType,
        purchase.Line,
        module='sale_cost_apply_purchase', type_='model')
