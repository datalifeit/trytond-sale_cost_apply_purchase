# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Line(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @classmethod
    def _get_origin(cls):
        result = super()._get_origin()
        result.append('sale.cost')
        return result
