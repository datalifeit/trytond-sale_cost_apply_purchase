=================================
Sale Cost Apply Purchase Scenario
=================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install sale_cost_apply_purchase::

    >>> config = activate_modules(['sale_cost_apply_purchase', 'sale_processing2confirmed'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

Create tax::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> address = customer.addresses.new()
    >>> address.name = 'Address 2'
    >>> customer.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> gram, = ProductUom.find([('name', '=', 'Gram')])
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'product 1'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product1 = template.products[0]

    >>> template = ProductTemplate()
    >>> template.name = 'product 2'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.purchasable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product2 = template.products[0]

    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('50')
    >>> template.cost_price = Decimal('20')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.purchasable = True
    >>> template.save()
    >>> service = template.products[0]

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create cost type invoice in::

    >>> CostType = Model.get('sale.cost.type')
    >>> type_invoice = CostType(name='Invoice')
    >>> type_invoice.product = service
    >>> type_invoice.formula = '0.2*unknown_amount'
    >>> type_invoice.apply_method = 'invoice_in'
    >>> type_invoice.formula = '0.2*untaxed_amount'
    >>> type_invoice.invoice_party = supplier
    >>> type_invoice.save()


Cost type change purchase state::

    >>> type_invoice.apply_method != 'purchase'
    True
    >>> type_invoice.purchase_state == None
    True
    >>> type_invoice.apply_method = 'purchase'
    >>> type_invoice.purchase_state == 'quotation'
    True
    >>> type_invoice.apply_method = 'invoice_in'
    >>> type_invoice.purchase_state == None
    True
    >>> type_invoice.save()


Create cost templates::

    >>> CostTemplate = Model.get('sale.cost.template')
    >>> template = CostTemplate()
    >>> template.type_ = type_invoice
    >>> template.invoice_party == supplier
    True
    >>> template.formula = '0.1*untaxed_amount'
    >>> template.party = customer
    >>> template.save()

Sale 2 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product2
    >>> sale_line.quantity = 300.0
    >>> sale_line.unit = gram
    >>> sale.click('quote')
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('29.00'), Decimal('2.90'), Decimal('31.90'))
    >>> len(sale.costs)
    1
    >>> sale_cost, = sale.costs
    >>> sale_cost.description = 'description'


Create cost type purchase::

    >>> type_invoice.apply_method = 'purchase'
    >>> type_invoice.product = service
    >>> type_invoice.purchase_state == 'quotation'
    True
    >>> type_invoice.save()


Check purchase lines::

    >>> SaleCost = Model.get('sale.cost')
    >>> sale_cost = SaleCost()
    >>> sale_cost.invoice_party = supplier
    >>> sale_cost.document = sale
    >>> sale_cost.type_ = type_invoice
    >>> sale_cost.template = template
    >>> sale_cost.description = 'description'
    >>> sale_cost.save()
    >>> bool(sale_cost.applied)
    False
    >>> bool(sale_cost.appliable)
    True
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '=', False)]))
    True
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '!=', False)]))
    False
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '=', True)]))
    False
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '!=', True)]))
    True
    >>> sale_cost.click('apply')
    >>> bool(sale_cost.applied)
    True
    >>> bool(sale_cost.appliable)
    False
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '=', False)]))
    False
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '!=', False)]))
    True
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '=', True)]))
    True
    >>> bool(SaleCost.find([('id', '=', sale_cost.id), ('applied', '!=', True)]))
    False
    >>> len([sale_cost.purchase])
    1
    >>> sale_cost.purchase.description == sale_cost.description
    True
    >>> PurchaseLine = Model.get('purchase.line')
    >>> line, = PurchaseLine.find([()])
    >>> line.origin == sale_cost
    True
    >>> line.purchase.state ==  type_invoice.purchase_state
    True
    >>> sale_cost.click('unapply')
    >>> sale_cost.reload()
    >>> lines = PurchaseLine.find([()])
    >>> len(lines)
    0
    >>> sale_cost.click('delete')


Check cost applying::

    >>> invoice_cost, = sale.costs
    >>> sale.sale_date
    >>> sale.click('confirm')
    >>> PurchaseLine = Model.get('purchase.line')
    >>> line, = PurchaseLine.find([()])
    >>> line.purchase.party == invoice_cost.invoice_party
    True
    >>> line.product == invoice_cost.type_.product
    True
    >>> line.purchase.company == sale.company
    True
    >>> line.unit_price == invoice_cost.amount
    True

Unprocess sale and check purchase still exists::

    >>> sale.click('unprocess')
    >>> lines = PurchaseLine.find([()])
    >>> len(lines)
    1

Unapply sale cost and check purchase line has been deleted::

    >>> invoice_cost.click('unapply')
    >>> lines = PurchaseLine.find([()])
    >>> len(lines)
    0

Check description 3 SaleCost same description::

    >>> sale_cost = SaleCost()
    >>> sale_cost.invoice_party = supplier
    >>> sale_cost.document = sale
    >>> sale_cost.type_ = type_invoice
    >>> sale_cost.template = template
    >>> sale_cost.description = 'description'
    >>> sale_cost.save()
    >>> sale_cost2 = SaleCost()
    >>> sale_cost2.invoice_party = supplier
    >>> sale_cost2.document = sale
    >>> sale_cost2.type_ = type_invoice
    >>> sale_cost2.template = template
    >>> sale_cost2.description = 'description'
    >>> sale_cost2.save()
    >>> sale_cost.type_.apply_point == 'on_confirm'
    True
    >>> sale_cost2.type_.apply_point == 'on_confirm'
    True
    >>> len(sale.costs)
    3
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.costs[0].purchase.description == sale_cost2.description
    True


Check !description::

    >>> Purchase = Model.get('purchase.purchase')
    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale_2 = Sale()
    >>> sale_2.party = customer
    >>> sale_2.payment_term = payment_term
    >>> sale_2.invoice_method = 'order'
    >>> sale_line = sale_2.lines.new()
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line = sale_2.lines.new()
    >>> sale_line.product = product2
    >>> sale_line.quantity = 300.0
    >>> sale_line.unit = gram
    >>> sale_2.click('quote')
    >>> len(sale_2.costs)
    1
    >>> sale_cost, = sale_2.costs
    >>> sale_cost.description = 'description'
    >>> sale_cost = SaleCost()
    >>> sale_cost.invoice_party = supplier
    >>> sale_cost.document = sale_2
    >>> sale_cost.type_ = type_invoice
    >>> sale_cost.template = template
    >>> sale_cost.description = 'description diff'
    >>> sale_cost.save()
    >>> len(Purchase.find([]))
    1
    >>> sale_2.click('quote')
    >>> sale_2.click('confirm')
    >>> sale_2.costs[0].purchase.description is None
    True

Don't create another purchase when sale is back to draft and set to confirm again::

    >>> sale_cost.click('apply')
    >>> pre_confirm_purchases = len(Purchase.find([]))
    >>> pre_confirm_purchases
    2
    >>> sale_2.click('draft')
    >>> sale_2.click('confirm')
    >>> len(Purchase.find([])) == pre_confirm_purchases
    True


Create cost type purchase state processing::

    >>> type_invoice.apply_method = 'purchase'
    >>> type_invoice.product = service
    >>> type_invoice.purchase_state = 'processing'
    >>> type_invoice.save()


Check purchase type processing::

    >>> sale_cost = SaleCost()
    >>> sale_cost.invoice_party = supplier
    >>> sale_cost.document = sale
    >>> sale_cost.type_ = type_invoice
    >>> sale_cost.template = template
    >>> sale_cost.description = 'description'
    >>> sale_cost.save()
    >>> sale_cost.click('apply')
    >>> len([sale_cost.purchase])
    1
    >>> sale_cost.purchase.state == type_invoice.purchase_state
    True